import React, {Fragment} from 'react';
import {Router} from '@reach/router';

import Navbar from '../navbar/navbar';
import Home from '../home/home';
import About from '../about/about';
import Projects from '../projects/projects';
import Contact from '../contact/contact';

import {withTheme} from '../theme/theme';

function App() {
  return (
    <Fragment>
      <Navbar/>
      <Router >
            <Home path="/"/> 
            <About path="About"/> 
            <Projects path="Projects"/> 
            <Contact path="Contact"/>
      </Router>
    </Fragment>
    
  );
}

export default withTheme(App);

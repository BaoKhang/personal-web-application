import React from "react";
import { Link} from '@reach/router';

import { makeStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
  root: {
    	flexGrow: 1,
  },
  menuButton: {
		color: "#c30928",
  },
  title: {	
		flexGrow: 1,
		color: "#c30928",
  },
  siteTitle: {
		marginRight: theme.spacing(2),
		color: "#c30928",
  }
}));

const Navbar = (props) => {
	/* 
		Input: props = {siteTitle} 
       	Output: navigation bar
    */

   	const [anchorEl, setAnchorEl] = React.useState(null);

   	const handleClick = (event) => {
	 	setAnchorEl(event.currentTarget);
   	};
 
   	const handleClose = () => {
	 	setAnchorEl(null);
   	};

	const classes = useStyles();

	return (
		<div className={classes.root}>
			<AppBar position="static" color="default" >
				<Toolbar>
				<Typography variant="h4" className={classes.title}>
					Bao Khang
				</Typography>
				
				<Typography variant="h6" className={classes.siteTitle}>
					{props.siteTitle}
				</Typography>
				<IconButton edge="start" className={classes.menuButton} aria-label="menu" onClick={handleClick}>
					<MenuIcon />
				</IconButton>
				<Menu
					id="content-menu"
					anchorEl={anchorEl}
					keepMounted
					open={Boolean(anchorEl)}
					onClose={handleClose}
				>
					<MenuItem onClick={handleClose} ><Link to="/"> Home </Link></MenuItem>
					<MenuItem onClick={handleClose} ><Link to="About"> About </Link></MenuItem>
					<MenuItem onClick={handleClose} ><Link to="Projects"> Projects </Link></MenuItem>
					<MenuItem onClick={handleClose} ><Link to="Contact"> Contact </Link></MenuItem>
				</Menu>
				</Toolbar>
			</AppBar>
		</div>
  	);
}

export default Navbar;
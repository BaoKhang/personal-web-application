import React from "react";

import {ThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
    }
});

const Theme = (props) => {
    const { children } = props;
    return (
    <ThemeProvider theme={theme}>
        <CssBaseline/>
        {children}
    </ThemeProvider>);
};

export const withTheme = (Component) => {
    return (props =>{
        return (
            <Theme>
                <Component {...props} />
            </Theme>
        );
    });
};
